import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {API_URL, PUBKEY} from '@/common/config'
import JSEncrypt from 'node-jsencrypt'
import sha1 from 'sha1'

const ApiService = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = API_URL
    Vue.axios.defaults.withCredentials = true
    this.encrypt = new JSEncrypt({default_key_size: 2048})
    this.encrypt.setPublicKey(PUBKEY)
  },

  login (user) {
    return Vue.axios.get('/login', '', {
      headers: {'Content-Type': 'text/plain', 'X-Requested-With': 'XMLHttpRequest'}
    }).then(resp => {
      const nonce = resp.data.nonce
      const data = {
        login: user.email,
        hash: sha1(sha1(user.password) + nonce)
      }

      const encrypted = this.encrypt.encrypt(JSON.stringify(data))

      console.log({user: user, nonce: nonce, data: data, encrypted: encrypted})

      return Vue.axios.post('/login', encrypted, {
        headers: {'Content-Type': 'text/plain', 'X-Requested-With': 'XMLHttpRequest'}
      })
    })
  },

  register (user) {
    const data = {
      username: user.username,
      email: user.email,
      password: user.password
    }

    const encrypted = this.encrypt.encrypt(JSON.stringify(data))
    return Vue.axios.post('/signup', encrypted, {
      headers: {'Content-Type': 'text/plain', 'X-Requested-With': 'XMLHttpRequest'}
    })
  },

  logout () {
    return Vue.axios.post('/logout', '', {
      withCredentials: true,
      headers: {'Content-Type': 'text/plain', 'X-Requested-With': 'XMLHttpRequest'}
    })
  },

  getInfo () {
    return Vue.axios.get('/info')
  },

  isLoggedIn () {
    return Vue.axios.get('/isloggedin')
  },

  /* Knowledge Archive */
  fetchGods () {
    return Vue.axios.get('/gods')
  },

  fetchGod (id) {
    return Vue.axios.get('/gods/' + id)
  },

  fetchReligions () {
    return Vue.axios.get('/religions')
  },

  fetchArtifacts () {
    return Vue.axios.get('/artifacts')
  },

  fetchPeople () {
    return Vue.axios.get('/people')
  },

  fetchHouses () {
    return Vue.axios.get('/houses')
  },

  fetchGeolocations () {
    return Vue.axios.get('/geolocations')
  },

  fetchOrganisations () {
    return Vue.axios.get('/organisations')
  },

  fetchEvents () {
    return Vue.axios.get('/events')
  }

}

export default ApiService
