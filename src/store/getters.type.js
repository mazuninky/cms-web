export const IS_AUTHENTICATED = 'isAuthenticated'
export const GET_CURRENT_USER = 'getCurrentUser'
export const GET_AUTH_ERRORS = 'getAuthErrors'

export const GET_ARCHIVE_POSTS = 'getArchivePosts'
export const GET_ARCHIVE_POSTS_CATEGORY = 'getArchivePostsCategory'
export const GET_ARCHIVE_POSTS_IS_LOADING = 'getArchivePostsIsLoading'

export const GET_POST = 'getPost'
export const GET_POSTS_IS_LOADING = 'getPostIsLoading'
