import {CHECK_AUTH} from './actions.type'
import {SET_AUTH, PURGE_AUTH, SET_ERROR} from './mutations.type'
import ApiService from '../common/api.service'
import { AUTH_CHECK_ERROR, SERVER_NOT_RESPONDING, ERROR_SENDING_REQUEST } from './errors'

export default {
  [CHECK_AUTH] ({commit, state}) {
    console.log('checking auth..')
    return new Promise((resolve, reject) => {
      ApiService.isLoggedIn()
      .then((resp) => {
        commit(SET_AUTH)
        resolve(true) // authenticated
      })
      .catch((err) => {
        if (err.response) {
          // response is not 2xx
          if (err.response.status === 401) {
            commit(PURGE_AUTH)
            resolve(false) // not authenticated
          } else {
            commit(SET_ERROR, AUTH_CHECK_ERROR)
            console.log('checking auth..')
          }
        } else if (err.request) {
          // reqeust was sent, but no response is available
          commit(SET_ERROR, SERVER_NOT_RESPONDING)
          console.log('checking auth..')
        } else {
          // request wasn't sent
          commit(SET_ERROR, ERROR_SENDING_REQUEST)
          console.log('checking auth..')
        }

        reject()
      })
    })
  }
}
