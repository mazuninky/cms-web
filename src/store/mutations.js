import {SET_AUTH, PURGE_AUTH, SET_ERROR} from './mutations.type'

export default {
  [SET_AUTH] (state) {
    state.user = {
      isAuthenticated: true,
      ...state.user
    }
    state.error = undefined
    console.log('set auth' + state.user)
  },
  [PURGE_AUTH] (state) {
    state.user = {
      isAuthenticated: false,
      ...state.user
    }
    state.error = undefined

    console.log('purge auth: ' + state.user)
  },
  [SET_ERROR] (state, error) {
    state.error = error
  }
}
