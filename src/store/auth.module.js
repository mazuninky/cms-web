import ApiService from '@/common/api.service'
import {GET_CURRENT_USER, IS_AUTHENTICATED, GET_AUTH_ERRORS} from './getters.type'
import {LOGIN, LOGOUT, SIGNUP} from './actions.type'
import {SET_AUTH, PURGE_AUTH, SET_ERROR} from './mutations.type'

const state = {
  errors: null,
  user: {},
  isAuthenticated: false
}

const getters = {
  [GET_CURRENT_USER] (state) {
    return state.user
  },
  [IS_AUTHENTICATED] (state) {
    return state.isAuthenticated
  },
  [GET_AUTH_ERRORS] (state) {
    return state.errors
  }
}

const actions = {
  [LOGIN] (context, credentials) {
    context.commit(SET_ERROR, null)
    return new Promise((resolve) => {
      ApiService
        .login(credentials)
        .then(({data}) => {
          context.commit(SET_AUTH)
          resolve(data)
        })
        .catch(({response}) => {
          context.commit(SET_ERROR, response)
        })
    })
  },
  [LOGOUT] (context) {
    context.commit(PURGE_AUTH)
  },
  [SIGNUP] (context, credentials) {
    return new Promise((resolve) => {
      ApiService
        .register(credentials)
        .then(({data}) => {
          // context.commit(SET_AUTH, data.access_token)
          resolve(data)
        })
        .catch(({response}) => {
          context.commit(SET_ERROR, response)
        })
    })
  }
}

const mutations = {
  [SET_ERROR] (state, error) {
    state.errors = error
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
